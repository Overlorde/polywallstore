import Vue from 'vue';
import Router from 'vue-router';
import HelloWorld from '@/components/HelloWorld';
import LoginPage from '@/components/LoginPage';
import Logout from '@/components/Logout';
import DepartmentManagersList from '@/components/DepartmentManagersList';
import StoresList from '@/components/StoresList';
import Products from '@/components/ProductsList';
import UserCreate from '@/components/UserCreate';

Vue.use(Router);

export default new Router({
 routes: [
   {
     path: '/',
     name: 'LoginPage',
     component: LoginPage
   },
   {
     path: '/comptes',
     name: 'DepartmentManagersList',
     component: DepartmentManagersList
   },
   {
     path: '/magasins',
     name: 'StoresList',
     component: StoresList
   },
   {
     path: '/produits',
     name: 'Products',
     component: Products
   },
   {
     path: '/ajoutCompte',
     name: 'UserCreate',
     component: UserCreate
   },
   {
      path: '/logout',
      name: 'Logout',
      component: Logout
    },
    {
       path: '/accueil',
       name: 'HelloWorld',
       component: HelloWorld
     },
   // otherwise redirect to home
  { path: '*', redirect: '/' }
 ]
});
