import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Menu from 'vue-sidebar-menu'
import 'vue-sidebar-menu/dist/vue-sidebar-menu.css'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import axios from './backend/vue-axios'
import store from './store'

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'


Vue.config.productionTip = false

Vue.use(Menu)

new Vue({
  render: h => h(App),
  router,
  axios,
  store,
}).$mount('#app')
