﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Storage;
using PolyWallStore.Models;
using PolyWallStoreApi.Controllers;
using PolyWallStoreApi.Data;
using PolyWallStoreApi.Models;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace PolyWallStoreApi.Tests
{
    public class UnitTestDepartmentsController
    {
        private readonly DbContextOptions<PolyWallStoreApiContext> options;
        public UnitTestDepartmentsController()
        {
            options = new DbContextOptionsBuilder<PolyWallStoreApiContext>()
            .UseInMemoryDatabase("test_database", new InMemoryDatabaseRoot()).ConfigureWarnings(x =>
            {
                x.Ignore(CoreEventId.ManyServiceProvidersCreatedWarning);
            })
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new PolyWallStoreApiContext(options))
            {
                DepartmentManager departmentManager1 = new DepartmentManager { Id = 1, FirstName = "firstname_user1", LastName = "lastname_user1", Email = "test_mail1@email.com" };
                DepartmentManager departmentManager2 = new DepartmentManager { Id = 2, FirstName = "firstname_user2", LastName = "lastname_user2", Email = "test_mail2@email.com" };
                DepartmentManager departmentManager3 = new DepartmentManager { Id = 3, FirstName = "firstname_user3", LastName = "lastname_user3", Email = "test_mail3@email.com" };

                Address address = new Address { Id = 1, City = "city", Street = "Street", ZipCode = 37000 };
                Product product = new Product { Id = 1, Icon = "http://icon.ico", Name = "Product_name", Price = 15.00 };
                StoreOwner storeOwner = new StoreOwner { Id = 4, FirstName = "first_name_storeowner", LastName = "last_name_storeowner", Email = "test_storeowner@email.com" };
                Store store = new Store { Id = 1, Name = "store", Address = address, StoreOwner = storeOwner };
                Department department1 = new Department { Id = 1, Name = "Department1", DepartmentManager = departmentManager1, Store = store };
                Department department2 = new Department { Id = 2, Name = "Department2", DepartmentManager = departmentManager2, Store = store };
                Department department3 = new Department { Id = 3, Name = "Department3", DepartmentManager = departmentManager3, Store = store };
                department1.Products.Add(product);
                department2.Products.Add(product);
                department3.Products.Add(product);
                context.Department.Add(department1);
                context.Department.Add(department2);
                context.Department.Add(department3);
                context.SaveChanges();
            }
        }
        [Fact]
        public async Task GetDepartment_Should_Return_All_Departments()
        {
            // Use a clean instance of the context to run the test
            using var context = new PolyWallStoreApiContext(options);
            var service = new DepartmentsController(context);
            var result = await service.GetDepartment();
            Assert.Equal(3, result.Value.ToList().Count); //check if what we want -> table same for departmentManager and storeOwner
            Assert.Equal("Department1", result.Value.ToList()[0].Name);
            Assert.Equal("Department2", result.Value.ToList()[1].Name);
            Assert.Equal("Department3", result.Value.ToList()[2].Name);
            Assert.Equal(1, result.Value.ToList()[0].Id);
        }

        [Fact]
        public async Task GetDepartment_Should_Return_A_Department()
        {
            // Use a clean instance of the context to run the test
            using var context = new PolyWallStoreApiContext(options);
            var service = new DepartmentsController(context);
            var result = await service.GetDepartment(1);
            Assert.Equal(1, result.Value.Id);
            Assert.Equal("Department1", result.Value.Name);

        }

        [Fact]
        public async Task GetDepartment_Should_Return_Null()
        {

            // Use a clean instance of the context to run the test
            using var context = new PolyWallStoreApiContext(options);
            var service = new DepartmentsController(context);
            var result = await service.GetDepartment(100);
            Assert.Null(result.Value);

        }

        [Fact]
        public async Task PostDepartment_Should_Post_A_Department()
        {

            // Use a clean instance of the context to run the test
            using var context = new PolyWallStoreApiContext(options);
            Store store = new Store { Id = 2, Name = "store2" };
            Product product = new Product { Id = 2, Icon = "http://icon2.ico", Name = "Product_name2", Price = 15.00 };
            DepartmentManager departmentManager = new DepartmentManager { Id = 4, FirstName = "firstname_user4", LastName = "lastname_user4", Email = "test_mail4@email.com" };
            var service = new DepartmentsController(context);
            Department department = new Department { Id = 4, Name = "Department1", DepartmentManager = departmentManager, Store = store };
            department.Products.Add(product);
            var result = await service.PostDepartment(department);
            Assert.IsType<CreatedAtActionResult>(result.Result);
        }
        [Fact]
        public async Task PutDepartment_Should_Put_A_Department()
        {
            // Use a clean instance of the context to run the test
            using var context = new PolyWallStoreApiContext(options);
            var service = new DepartmentsController(context);
            Department DepartmentManager = new Department { Id = 1, Name = "Department1 has been modified" };
            var result = await service.PutDepartment(1, DepartmentManager);
            Assert.Equal(service.GetDepartment(1).Result.Value.Name, DepartmentManager.Name);
        }
    }
}
