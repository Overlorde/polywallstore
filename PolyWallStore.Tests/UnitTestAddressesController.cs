﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using PolyWallStoreApi.Data;
using PolyWallStoreApi.Models;
using PolyWallStore.Models;
using Xunit;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Diagnostics;

namespace PolyWallStoreApi.Tests
{
    public class UnitTestAddressesController
    {
        private readonly DbContextOptions<PolyWallStoreApiContext> _options;


        public UnitTestAddressesController()
        {

            _options = new DbContextOptionsBuilder<PolyWallStoreApiContext>()
            .UseInMemoryDatabase("test_database", new InMemoryDatabaseRoot()).ConfigureWarnings(x =>
            {
                x.Ignore(CoreEventId.ManyServiceProvidersCreatedWarning);
            })
            .Options;

            // Insert seed data into the database using one instance of the context
            using var context = new PolyWallStoreApiContext(_options);
            Department department = new Department { Id = 1, Name = "Departement1" };
            Address address = new Address { Id = 1, City = "city", Street = "Street", ZipCode = 37000 };
            StoreOwner storeOwner = new StoreOwner { Id = 4, FirstName = "first_name_storeowner", LastName = "last_name_storeowner", Email = "test_storeowner@email.com" };
            Store store = new Store { Id = 1, Name = "store", Address = address, StoreOwner = storeOwner };

            context.Address.Add(new Address { Id = 1, City = "city1", Street = "Street1", ZipCode = 37200 });
            context.Address.Add(new Address { Id = 2, City = "city2", Street = "Street2", ZipCode = 37100, Store = store });
            context.Address.Add(new Address { Id = 3, City = "city3", Street = "Street3", ZipCode = 37300 });
            context.SaveChanges();
        }
        [Fact]
        public void GetAddress_Should_Return_All_Addresses()
        {

            // Use a clean instance of the context to run the test
            using var context = new PolyWallStoreApiContext(_options);
            var service = new AddressesController(context);
            var result = service.GetAddress();
            Assert.Equal(3, result.Result.Value.ToList().Count);
            Assert.Equal("city1", result.Result.Value.ToList()[0].City);
            Assert.Equal("city2", result.Result.Value.ToList()[1].City);
            Assert.Equal("city3", result.Result.Value.ToList()[2].City);
            Assert.Equal(1, result.Result.Value.ToList()[0].Id);
        }

        [Fact]
        public async Task GetAddress_Should_Return_An_Address()
        {
            // Use a clean instance of the context to run the test
            using var context = new PolyWallStoreApiContext(_options);
            var service = new AddressesController(context);
            var result = await service.GetAddress(1);
            Assert.Equal(1, result.Value.Id);
            Assert.Equal("city1", result.Value.City);

        }

        [Fact]
        public async Task GetAddress_Should_Return_Null()
        {

            // Use a clean instance of the context to run the test
            using var context = new PolyWallStoreApiContext(_options);
            var service = new AddressesController(context);
            var result = await service.GetAddress(100);
            Assert.Null(result.Value);

        }

        [Fact]
        public async Task PostAddress_Should_Post_An_Address()
        {

            // Use a clean instance of the context to run the test
            using var context = new PolyWallStoreApiContext(_options);
            var service = new AddressesController(context);
            Address Address = new Address { Id = 4, City = "city4" };
            var result = await service.PostAddress(Address);
            Assert.IsType<CreatedAtActionResult>(result.Result);
        }
        [Fact]
        public async Task PutAddress_Should_Put_An_Address()
        {

            // Use a clean instance of the context to run the test
            using var context = new PolyWallStoreApiContext(_options);
            var service = new AddressesController(context);
            Address Address = new Address { Id = 1, City = "city1" };
            var result = await service.PutAddress(1, Address);
            Assert.Equal(service.GetAddress(1).Result.Value.City, Address.City);
        }
    }
}
