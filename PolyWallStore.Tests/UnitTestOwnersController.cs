﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Moq;
using PolyWallStore.Models;
using PolyWallStoreApi.Controllers;
using PolyWallStoreApi.Data;
using PolyWallStoreApi.ExposeModels;
using PolyWallStoreApi.Helper;
using PolyWallStoreApi.Models;
using PolyWallStoreApi.Services;
using Xunit;

namespace PolyWallStoreApi.Tests
{
    public class UnitTestOwnersController
    {
        public static IConfiguration InitConfiguration()
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.Development.json")
                .Build();
            return config;
        }

        private readonly DbContextOptions<PolyWallStoreApiContext> _options;
        private readonly Mock<IUserService> _userService;
        private readonly IMapper _mapper;
        private readonly IOptions<AppSettings> _appSettings;

        public UnitTestOwnersController()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<DepartmentManager, UserModel>();
                cfg.CreateMap<RegisterDepartmentManager, DepartmentManager>();
                cfg.CreateMap<UpdateDepartmentManager, DepartmentManager>();
                cfg.CreateMap<StoreOwner, UserModel>();
                cfg.CreateMap<RegisterStoreOwner, StoreOwner>();
                cfg.CreateMap<UpdateStoreOwner, StoreOwner>();
                cfg.CreateMap<Owner, UserModel>();
                cfg.CreateMap<RegisterOwner, Owner>();
                cfg.CreateMap<UpdateOwner, Owner>();
            });

            _mapper = config.CreateMapper();
            _userService = new Mock<IUserService>();
            _options = new DbContextOptionsBuilder<PolyWallStoreApiContext>()
            .UseInMemoryDatabase("test_database", new InMemoryDatabaseRoot()).ConfigureWarnings(x =>
            {
                x.Ignore(CoreEventId.ManyServiceProvidersCreatedWarning);
            })
                .Options;
            _appSettings = Options.Create(new AppSettings());

            // Insert seed data into the database using one instance of the context
            using var context = new PolyWallStoreApiContext(_options);
            Owner owner1 = new Owner { Id = 1, FirstName = "firstname_user1", LastName = "lastname_user1", Email = "test_mail1@email.com" };
            Owner owner2 = new Owner { Id = 2, FirstName = "firstname_user2", LastName = "lastname_user2", Email = "test_mail2@email.com" };
            Owner owner3 = new Owner { Id = 3, FirstName = "firstname_user3", LastName = "lastname_user3", Email = "test_mail3@email.com" };

            Address address = new Address { Id = 1, City = "city", Street = "Street", ZipCode = 37000 };

            Product product = new Product { Id = 1, Icon = "http://icon.ico", Name = "Product_name", Price = 15.00 };
            StoreOwner storeOwner = new StoreOwner { Id = 5, FirstName = "first_name_storeowner", LastName = "last_name_storeowner", Email = "test_storeowner@email.com" };
            Store store1 = new Store { Id = 1, Name = "store1", Address = address, Owner = owner1 };
            Store store2 = new Store { Id = 2, Name = "store2", Address = address, Owner = owner2 };
            Store store3 = new Store { Id = 3, Name = "store3", Address = address, Owner = owner3 };
            context.StoreOwner.Add(storeOwner);
            context.Store.Add(store1);
            context.Owner.Add(owner1);
            context.Owner.Add(owner2);
            context.Owner.Add(owner3);
            DepartmentManager departmentManager = new DepartmentManager { Id = 4, FirstName = "firstname_user1", LastName = "lastname_user1", Email = "test_mail1@email.com" };
            Department department = new Department { Id = 1, Name = "Department1", DepartmentManager = departmentManager, Store = store1 };
            context.SaveChanges();
            _userService.Setup(repo => repo.GetAllOwner()).Returns(context.Owner);
            _userService.Setup(repo => repo.GetOwnerById(1)).Returns(context.Owner.Find(1));
            _userService.Setup(repo => repo.GetOwnerById(2)).Returns(context.Owner.Find(2));
            _userService.Setup(repo => repo.GetOwnerById(3)).Returns(context.Owner.Find(3));

        }
        [Fact]
        public void GetOwner_Should_Return_All_Owners()
        {
            // Use a clean instance of the context to run the test
            var service = new OwnersController(_userService.Object, _mapper, _appSettings);
            //var result = service.GetOwner();
            ////Assert
            //var okObjectResult = result as OkObjectResult;
            //Assert.NotNull(okObjectResult);
            //var model = okObjectResult.Value as List<UserModel>;
            //Assert.NotNull(model);

            //Assert.Equal("firstname_user1", model[0].FirstName);
            //Assert.Equal("firstname_user2", model[1].FirstName);
        }

        [Fact]
        public void GetOwner_Should_Return_A_Owner()
        {
            // Use a clean instance of the context to run the test
            var service = new OwnersController(_userService.Object, _mapper, _appSettings);
            var result = service.GetOwner(1);
            //Assert
            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);
            var model = okObjectResult.Value as UserModel;
            Assert.NotNull(model);

            Assert.Equal("firstname_user1", model.FirstName);
        }

        [Fact]
        public void GetOwner_Should_Return_Null()
        {

            // Use a clean instance of the context to run the test
            var service = new OwnersController(_userService.Object, _mapper, _appSettings);
            var result = service.GetOwner(100);
            //Assert
            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);
            var model = okObjectResult.Value as UserModel;
            Assert.Null(model);
        }

        [Fact]
        public void PostOwner_Should_Post_A_Owner()
        {
            // Use a clean instance of the context to run the test
            Store store = new Store { Id = 2, Name = "store2" };
            Product product = new Product { Id = 2, Icon = "http://icon2.ico", Name = "Product_name2", Price = 15.00 };
            DepartmentManager departmentManager = new DepartmentManager { Id = 4, FirstName = "firstname_user4", LastName = "lastname_user4", Email = "test_mail4@email.com" };
            Department department = new Department { Id = 4, Name = "Department1", DepartmentManager = departmentManager, Store = store };
            department.Products.Add(product);
            RegisterOwner registerOwner = new RegisterOwner { FirstName = "Owner_firstname", LastName = "Owner_lastname", Email = "owner_email@email.com" };
            var service = new OwnersController(_userService.Object, _mapper, _appSettings);
            var result = service.PostOwner(registerOwner);
            //Assert
            var okObjectResult = result as StatusCodeResult;
            Assert.Equal(200, okObjectResult.StatusCode);
        }
        [Fact]
        public void PutOwner_Should_Put_A_Owner()
        {
            // Use a clean instance of the context to run the test
            var service = new OwnersController(_userService.Object, _mapper, _appSettings);
            UpdateOwner updateOwner = new UpdateOwner(null) { FirstName = "Owner_firstname1 has been modified" };
            var result = service.PutOwner(1, updateOwner);
            //Assert
            var okObjectResult = result as StatusCodeResult;
            Assert.Equal(200, okObjectResult.StatusCode);
        }
    }
}
