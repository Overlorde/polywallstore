﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Moq;
using PolyWallStore.Models;
using PolyWallStoreApi.Controllers;
using PolyWallStoreApi.Data;
using PolyWallStoreApi.ExposeModels;
using PolyWallStoreApi.Helper;
using PolyWallStoreApi.Models;
using PolyWallStoreApi.Services;
using Xunit;
namespace PolyWallStoreApi.Tests
{
    public class UnitTestDepartmentManagersController
    {
        public static IConfiguration InitConfiguration()
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.Development.json")
                .Build();
            return config;
        }

        private readonly DbContextOptions<PolyWallStoreApiContext> _options;
        private readonly Mock<IUserService> _userService;
        private readonly IMapper _mapper;
        private readonly IOptions<AppSettings> _appSettings;

        public UnitTestDepartmentManagersController()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<DepartmentManager, UserModel>();
                cfg.CreateMap<RegisterDepartmentManager, DepartmentManager>();
                cfg.CreateMap<UpdateDepartmentManager, DepartmentManager>();
                cfg.CreateMap<StoreOwner, UserModel>();
                cfg.CreateMap<RegisterStoreOwner, StoreOwner>();
                cfg.CreateMap<UpdateStoreOwner, StoreOwner>();
                cfg.CreateMap<Owner, UserModel>();
                cfg.CreateMap<RegisterOwner, Owner>();
                cfg.CreateMap<UpdateOwner, Owner>();
            });

            _mapper = config.CreateMapper();
            _userService = new Mock<IUserService>();
            _options = new DbContextOptionsBuilder<PolyWallStoreApiContext>()
            .UseInMemoryDatabase("test_database", new InMemoryDatabaseRoot()).ConfigureWarnings(x =>
            {
                x.Ignore(CoreEventId.ManyServiceProvidersCreatedWarning);
            })
            .Options;
            _appSettings = Options.Create(new AppSettings());

            // Insert seed data into the database using one instance of the context
            using var context = new PolyWallStoreApiContext(_options);
            Department department = new Department { Id = 1, Name = "Departement1" };
            Address address = new Address { Id = 1, City = "city", Street = "Street", ZipCode = 37000 };
            StoreOwner storeOwner = new StoreOwner { Id = 4, FirstName = "first_name_storeowner", LastName = "last_name_storeowner", Email = "test_storeowner@email.com" };
            Store store = new Store { Id = 1, Name = "store", Address = address, StoreOwner = storeOwner };

            context.DepartmentManager.Add(new DepartmentManager { Id = 1, FirstName = "firstname_user1", LastName = "lastname_user1", Email = "test_mail1@email.com" });
            context.DepartmentManager.Add(new DepartmentManager { Id = 2, FirstName = "firstname_user2", LastName = "lastname_user2", Email = "test_mail2@email.com", Department = department, Store = store });
            context.DepartmentManager.Add(new DepartmentManager { Id = 3, FirstName = "firstname_user3", LastName = "lastname_user3", Email = "test_mail3@email.com" });
            context.SaveChanges();
            _userService.Setup(repo => repo.GetAllDepartmentManager()).Returns(context.DepartmentManager);
            _userService.Setup(repo => repo.GetDepartmentManagerById(1)).Returns(context.DepartmentManager.Find(1));
            _userService.Setup(repo => repo.GetDepartmentManagerById(2)).Returns(context.DepartmentManager.Find(2));
            _userService.Setup(repo => repo.GetDepartmentManagerById(3)).Returns(context.DepartmentManager.Find(3));
        }
        [Fact]
        public void GetDepartmentManager_Should_Return_All_DepartmentManager()
        {

            var service = new DepartmentManagersController(_userService.Object, _mapper, _appSettings);
            //var result = service.GetDepartmentManager();
            ////Assert
            //var okObjectResult = result as OkObjectResult;
            //Assert.NotNull(okObjectResult);
            //var model = okObjectResult.Value as List<UserModel>;
            //Assert.NotNull(model);

            //Assert.Equal("firstname_user1", model[0].FirstName);
            //Assert.Equal("firstname_user2", model[1].FirstName);
        }

        [Fact]
        public void GetDepartmentManager_Should_Return_A_DepartmentManager()
        {
            var service = new DepartmentManagersController(_userService.Object, _mapper, _appSettings);
            var result = service.GetDepartmentManager(1);
            //Assert
            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);
            var model = okObjectResult.Value as UserModel;
            Assert.NotNull(model);

            Assert.Equal("firstname_user1", model.FirstName);

        }

        [Fact]
        public void GetDepartmentManager_Should_Return_Null()
        {

            var service = new DepartmentManagersController(_userService.Object, _mapper, _appSettings);
            var result = service.GetDepartmentManager(100);
            //Assert
            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);
            var model = okObjectResult.Value as UserModel;
            Assert.Null(model);
        }

        [Fact]
        public void PostDepartmentManager_Should_Post_A_DepartmentManager()
        {

            var service = new DepartmentManagersController(_userService.Object, _mapper, _appSettings);
            RegisterDepartmentManager DepartmentManager = new RegisterDepartmentManager { FirstName = "first_name4", LastName = "last_name4" };
            var result = service.Register(DepartmentManager);
            //Assert
            var okObjectResult = result as StatusCodeResult;
            Assert.Equal(200, okObjectResult.StatusCode);

        }
        [Fact]
        public void PutDepartmentManager_Should_Put_A_DepartmentManager()
        {

            var service = new DepartmentManagersController(_userService.Object, _mapper, _appSettings);
            UpdateDepartmentManager DepartmentManager = new UpdateDepartmentManager { FirstName = "first_name1 has been modified" };
            var result = service.PutDepartmentManager(1, DepartmentManager);
            //Assert
            var okObjectResult = result as StatusCodeResult;
            Assert.Equal(200, okObjectResult.StatusCode);
        }
    }
}
