using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Storage;
using PolyWallStore.Models;
using PolyWallStoreApi.Controllers;
using PolyWallStoreApi.Data;
using PolyWallStoreApi.Models;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace PolyWallStore.Tests
{
    public class UnitTestStoresController
    {
        private readonly DbContextOptions<PolyWallStoreApiContext> _options;
        public UnitTestStoresController()
        {
            _options = new DbContextOptionsBuilder<PolyWallStoreApiContext>()
            .UseInMemoryDatabase("test_database", new InMemoryDatabaseRoot()).ConfigureWarnings(x =>
            {
                x.Ignore(CoreEventId.ManyServiceProvidersCreatedWarning);
            })
                .Options;

            // Insert seed data into the database using one instance of the context
            using var context = new PolyWallStoreApiContext(_options);
            Address address = new Address { Id = 1, City = "city1", Street = "Street1", ZipCode = 37000 };
            DepartmentManager departmentManager = new DepartmentManager { Id = 1, FirstName = "first_name_departmentManager", LastName = "last_name_departmentManager", Email = "department_manager@email.com" };
            StoreOwner storeOwner = new StoreOwner { Id = 2, FirstName = "first_name", LastName = "last_name", Email = "email_storeOwner@email.com" };
            Department department = new Department { Id = 1, Name = "Departement1", DepartmentManager = departmentManager };
            Product product = new Product { Id = 1, Icon = "http://icon.png", Name = "product_name", Price = 15.00, Department = department };
            department.Products.Add(product); //ICollection must be readonly to prevent hack
            Store store = new Store { Id = 50, Name = "store1", Address = address, StoreOwner = storeOwner };

            store.DepartmentManagers.Add(departmentManager);
            store.Departments.Add(department);
            context.Store.Add(store);
            context.Store.Add(new Store { Id = 51, Name = "store2" });
            context.Store.Add(new Store { Id = 52, Name = "store3" });
            context.SaveChanges();
        }
        [Fact]
        public async Task GetStore_Should_Return_All_Stores()
        {

            // Use a clean instance of the context to run the test
            using var context = new PolyWallStoreApiContext(_options);
            var service = new StoresController(context);
            var result = await service.GetStore();
            Assert.Equal(3, result.Value.ToList().Count);
            Assert.Equal("store1", result.Value.ToList()[0].Name);
            Assert.Equal("store2", result.Value.ToList()[1].Name);
            Assert.Equal("store3", result.Value.ToList()[2].Name);
            Assert.Equal(50, result.Value.ToList()[0].Id);
        }

        [Fact]
        public async Task GetStore_Should_Return_A_Store()
        {

            // Use a clean instance of the context to run the test
            using var context = new PolyWallStoreApiContext(_options);
            var service = new StoresController(context);
            var result = await service.GetStore(50);
            Assert.Equal(50, result.Value.Id);
            Assert.Equal("store1", result.Value.Name);

        }

        [Fact]
        public async Task GetStore_Should_Return_Null()
        {

            // Use a clean instance of the context to run the test
            using var context = new PolyWallStoreApiContext(_options);
            var service = new StoresController(context);
            var result = await service.GetStore(100);
            Assert.Null(result.Value);

        }

        [Fact]
        public async Task PostStore_Should_Post_A_Store()
        {

            // Use a clean instance of the context to run the test
            using var context = new PolyWallStoreApiContext(_options);
            var service = new StoresController(context);
            Store Store = new Store() { Id = 53, Name = "this is a fake store" };
            var result = await service.PostStore(Store);
            Assert.IsType<CreatedAtActionResult>(result.Result);
        }
    }
}
