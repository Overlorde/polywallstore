﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Moq;
using PolyWallStore.Models;
using PolyWallStoreApi.Controllers;
using PolyWallStoreApi.Data;
using PolyWallStoreApi.ExposeModels;
using PolyWallStoreApi.Helper;
using PolyWallStoreApi.Models;
using PolyWallStoreApi.Services;
using Xunit;

namespace PolyWallStoreApi.Tests
{
    public class UnitTestStoreOwnersController
    {
        public static IConfiguration InitConfiguration()
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.Development.json")
                .Build();
            return config;
        }

        private readonly DbContextOptions<PolyWallStoreApiContext> _options;
        private readonly Mock<IUserService> _userService;
        private readonly IMapper _mapper;
        private readonly IOptions<AppSettings> _appSettings;
        public UnitTestStoreOwnersController()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<DepartmentManager, UserModel>();
                cfg.CreateMap<RegisterDepartmentManager, DepartmentManager>();
                cfg.CreateMap<UpdateDepartmentManager, DepartmentManager>();
                cfg.CreateMap<StoreOwner, UserModel>();
                cfg.CreateMap<RegisterStoreOwner, StoreOwner>();
                cfg.CreateMap<UpdateStoreOwner, StoreOwner>();
                cfg.CreateMap<Owner, UserModel>();
                cfg.CreateMap<RegisterOwner, Owner>();
                cfg.CreateMap<UpdateOwner, Owner>();
            });

            _mapper = config.CreateMapper();
            _userService = new Mock<IUserService>();
            _options = new DbContextOptionsBuilder<PolyWallStoreApiContext>()
            .UseInMemoryDatabase("test_database", new InMemoryDatabaseRoot()).ConfigureWarnings(x =>
            {
                x.Ignore(CoreEventId.ManyServiceProvidersCreatedWarning);
            })
            .Options;
            _appSettings = Options.Create(new AppSettings());

            // Insert seed data into the database using one instance of the context
            using var context = new PolyWallStoreApiContext(_options);
            Address address = new Address { Id = 1, City = "city", Street = "Street", ZipCode = 37000 };
            StoreOwner storeOwner1 = new StoreOwner { Id = 1, FirstName = "first_name_storeowner1", LastName = "last_name_storeowner1", Email = "test_storeowner1@email.com" };
            StoreOwner storeOwner2 = new StoreOwner { Id = 2, FirstName = "first_name_storeowner2", LastName = "last_name_storeowner2", Email = "test_storeowner2@email.com" };
            StoreOwner storeOwner3 = new StoreOwner { Id = 3, FirstName = "first_name_storeowner3", LastName = "last_name_storeowner3", Email = "test_storeowner3@email.com" };

            Store store1 = new Store { Id = 1, Name = "store1", Address = address, StoreOwner = storeOwner1 };
            Store store2 = new Store { Id = 2, Name = "store2", Address = address, StoreOwner = storeOwner2 };
            Store store3 = new Store { Id = 3, Name = "store3", Address = address, StoreOwner = storeOwner3 };
            DepartmentManager departmentManager = new DepartmentManager { Id = 1, FirstName = "firstname_user1", LastName = "lastname_user1", Email = "test_mail1@email.com" };
            Department department = new Department { Id = 1, Name = "Department3", DepartmentManager = departmentManager, Store = store1 };

            context.StoreOwner.Add(storeOwner1);
            context.StoreOwner.Add(storeOwner2);
            context.StoreOwner.Add(storeOwner3);
            context.SaveChanges();
            _userService.Setup(repo => repo.GetAllStoreOwner()).Returns(context.StoreOwner);
            _userService.Setup(repo => repo.GetStoreOwnerById(1)).Returns(context.StoreOwner.Find(1));
            _userService.Setup(repo => repo.GetStoreOwnerById(2)).Returns(context.StoreOwner.Find(2));
            _userService.Setup(repo => repo.GetStoreOwnerById(3)).Returns(context.StoreOwner.Find(3));
        }
        [Fact]
        public void GetStoreOwner_Should_Return_All_StoreOwners()
        {
            // Use a clean instance of the context to run the test
            var service = new StoreOwnersController(_userService.Object, _mapper, _appSettings);
            //var result = service.GetDepartmentManager();
            ////Assert
            //var okObjectResult = result as OkObjectResult;
            //Assert.NotNull(okObjectResult);
            //var model = okObjectResult.Value as List<UserModel>;
            //Assert.NotNull(model);

            //Assert.Equal("firstname_user1", model[0].FirstName);
            //Assert.Equal("firstname_user2", model[1].FirstName);
        }

        [Fact]
        public void GetStoreOwner_Should_Return_A_StoreOwner()
        {
            // Use a clean instance of the context to run the test
            var service = new StoreOwnersController(_userService.Object, _mapper, _appSettings);
            var result = service.GetStoreOwner(1);
            //Assert
            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);
            var model = okObjectResult.Value as UserModel;
            Assert.NotNull(model);

            Assert.Equal("first_name_storeowner1", model.FirstName);
        }

        [Fact]
        public void GetStoreOwner_Should_Return_Null()
        {

            // Use a clean instance of the context to run the test
            var service = new StoreOwnersController(_userService.Object, _mapper, _appSettings);
            var result = service.GetStoreOwner(100);
            //Assert
            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);
            var model = okObjectResult.Value as UserModel;
            Assert.Null(model);
        }

        [Fact]
        public void PostStoreOwner_Should_Post_A_StoreOwner()
        {

            // Use a clean instance of the context to run the test
            Store store = new Store { Id = 2, Name = "store2" };
            RegisterStoreOwner registerStoreOwner = new RegisterStoreOwner { FirstName = "first_name_storeowner4", LastName = "last_name_storeowner4", Email = "test_storeowner4@email.com" };
            Product product = new Product { Id = 2, Icon = "http://icon2.ico", Name = "Product_name2", Price = 15.00 };
            DepartmentManager departmentManager = new DepartmentManager { Id = 4, FirstName = "firstname_user4", LastName = "lastname_user4", Email = "test_mail4@email.com" };
            Department department = new Department { Id = 4, Name = "Department1", DepartmentManager = departmentManager, Store = store };
            department.Products.Add(product);
            var service = new StoreOwnersController(_userService.Object, _mapper, _appSettings);
            var result = service.PostStoreOwner(registerStoreOwner);
            //Assert
            var okObjectResult = result as StatusCodeResult;
            Assert.Equal(200, okObjectResult.StatusCode);
        }
        [Fact]
        public void PutStoreOwner_Should_Put_A_StoreOwner()
        {

            // Use a clean instance of the context to run the test
            var service = new StoreOwnersController(_userService.Object, _mapper, _appSettings);
            UpdateStoreOwner UpdateStoreOwner = new UpdateStoreOwner { FirstName = "firstname_storeowner1 has been modified" };
            var result = service.PutStoreOwner(1, UpdateStoreOwner);
            //Assert
            var okObjectResult = result as StatusCodeResult;
            Assert.Equal(200, okObjectResult.StatusCode);
        }
    }
}
