﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Storage;
using PolyWallStore.Models;
using PolyWallStoreApi.Controllers;
using PolyWallStoreApi.Data;
using PolyWallStoreApi.Models;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace PolyWallStoreApi.Tests
{
    public class UnitTestProductsController
    {
        private readonly DbContextOptions<PolyWallStoreApiContext> _options;
        public UnitTestProductsController()
        {
            _options = new DbContextOptionsBuilder<PolyWallStoreApiContext>()
            .UseInMemoryDatabase("test_database", new InMemoryDatabaseRoot()).ConfigureWarnings(x =>
            {
                x.Ignore(CoreEventId.ManyServiceProvidersCreatedWarning);
            })
                .Options;

            // Insert seed data into the database using one instance of the context
            using var context = new PolyWallStoreApiContext(_options);
            Address address = new Address { Id = 1, City = "city", Street = "Street", ZipCode = 37000 };
            Store store = new Store { Id = 1, Name = "store", Address = address };
            DepartmentManager departmentManager = new DepartmentManager { Id = 1, FirstName = "firstname_user1", LastName = "lastname_user1", Email = "test_mail1@email.com" };
            Department department = new Department { Id = 3, Name = "Department3", DepartmentManager = departmentManager, Store = store };
            Product product1 = new Product { Id = 1, Name = "Product1", Department = department, Icon = "http://icon1.ico", Price = 15.00 };
            Product product2 = new Product { Id = 2, Name = "Product2", Department = department, Icon = "http://icon2.ico", Price = 25.00 };
            Product product3 = new Product { Id = 3, Name = "Product3", Department = department, Icon = "http://icon3.ico", Price = 35.00 };

            context.Product.Add(product1);
            context.Product.Add(product2);
            context.Product.Add(product3);
            context.SaveChanges();
        }
        [Fact]
        public async Task GetProduct_Should_Return_All_Products()
        {
            // Use a clean instance of the context to run the test
            using var context = new PolyWallStoreApiContext(_options);
            var service = new ProductsController(context);
            var result = await service.GetProduct();
            Assert.Equal(3, result.Value.ToList().Count);
            Assert.Equal("Product1", result.Value.ToList()[0].Name);
            Assert.Equal("Product2", result.Value.ToList()[1].Name);
            Assert.Equal("Product3", result.Value.ToList()[2].Name);
            Assert.Equal(1, result.Value.ToList()[0].Id);
        }

        [Fact]
        public async Task GetProduct_Should_Return_A_Product()
        {
            // Use a clean instance of the context to run the test
            using var context = new PolyWallStoreApiContext(_options);
            var service = new ProductsController(context);
            var result = await service.GetProduct(1);
            Assert.Equal(1, result.Value.Id);
            Assert.Equal("Product1", result.Value.Name);

        }

        [Fact]
        public async Task GetProduct_Should_Return_Null()
        {

            // Use a clean instance of the context to run the test
            using var context = new PolyWallStoreApiContext(_options);
            var service = new ProductsController(context);
            var result = await service.GetProduct(100);
            Assert.Null(result.Value);

        }

        [Fact]
        public async Task PostProduct_Should_Post_A_Product()
        {

            // Use a clean instance of the context to run the test
            using var context = new PolyWallStoreApiContext(_options);
            Store store = new Store { Id = 2, Name = "store2" };
            Product product = new Product { Id = 4, Icon = "http://icon2.ico", Name = "Product_name2", Price = 15.00 };
            DepartmentManager departmentManager = new DepartmentManager { Id = 4, FirstName = "firstname_user4", LastName = "lastname_user4", Email = "test_mail4@email.com" };
            var service = new ProductsController(context);
            Department department = new Department { Id = 4, Name = "Department1", DepartmentManager = departmentManager, Store = store };
            department.Products.Add(product);
            var result = await service.PostProduct(product);
            Assert.IsType<CreatedAtActionResult>(result.Result);
        }
        [Fact]
        public async Task PutProduct_Should_Put_A_Product()
        {

            // Use a clean instance of the context to run the test
            using var context = new PolyWallStoreApiContext(_options);
            var service = new ProductsController(context);
            Product product = new Product { Id = 1, Name = "Product_name2 has been modified" };
            var result = await service.PutProduct(1, product);
            Assert.Equal(service.GetProduct(1).Result.Value.Name, product.Name);
        }
    }
}
