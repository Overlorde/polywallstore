USE [master]
GO
/****** Object:  Database [IntegrationDatabase]    Script Date: 17/01/2020 11:08:46 ******/
CREATE DATABASE [IntegrationDatabase]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'IntegrationDatabase', FILENAME = N'C:\Users\L-TITANTµ\AppData\Local\Microsoft\Microsoft SQL Server Local DB\Instances\MSSQLLocalDB\IntegrationDatabase.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'IntegrationDatabase_log', FILENAME = N'C:\Users\L-TITANTµ\AppData\Local\Microsoft\Microsoft SQL Server Local DB\Instances\MSSQLLocalDB\IntegrationDatabase.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [IntegrationDatabase] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [IntegrationDatabase].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [IntegrationDatabase] SET ANSI_NULL_DEFAULT ON 
GO
ALTER DATABASE [IntegrationDatabase] SET ANSI_NULLS ON 
GO
ALTER DATABASE [IntegrationDatabase] SET ANSI_PADDING ON 
GO
ALTER DATABASE [IntegrationDatabase] SET ANSI_WARNINGS ON 
GO
ALTER DATABASE [IntegrationDatabase] SET ARITHABORT ON 
GO
ALTER DATABASE [IntegrationDatabase] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [IntegrationDatabase] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [IntegrationDatabase] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [IntegrationDatabase] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [IntegrationDatabase] SET CURSOR_DEFAULT  LOCAL 
GO
ALTER DATABASE [IntegrationDatabase] SET CONCAT_NULL_YIELDS_NULL ON 
GO
ALTER DATABASE [IntegrationDatabase] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [IntegrationDatabase] SET QUOTED_IDENTIFIER ON 
GO
ALTER DATABASE [IntegrationDatabase] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [IntegrationDatabase] SET  DISABLE_BROKER 
GO
ALTER DATABASE [IntegrationDatabase] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [IntegrationDatabase] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [IntegrationDatabase] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [IntegrationDatabase] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [IntegrationDatabase] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [IntegrationDatabase] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [IntegrationDatabase] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [IntegrationDatabase] SET RECOVERY FULL 
GO
ALTER DATABASE [IntegrationDatabase] SET  MULTI_USER 
GO
ALTER DATABASE [IntegrationDatabase] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [IntegrationDatabase] SET DB_CHAINING OFF 
GO
ALTER DATABASE [IntegrationDatabase] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [IntegrationDatabase] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [IntegrationDatabase] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [IntegrationDatabase] SET QUERY_STORE = OFF
GO
USE [IntegrationDatabase]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [IntegrationDatabase]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 17/01/2020 11:08:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Address]    Script Date: 17/01/2020 11:08:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Address](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[City] [nvarchar](50) NOT NULL,
	[Street] [nvarchar](50) NOT NULL,
	[Zipcode] [int] NOT NULL,
	[StoreId] [int] NULL,
 CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Department]    Script Date: 17/01/2020 11:08:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Department](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[StoreId] [int] NULL,
	[DepartmentManagerId] [int] NULL,
 CONSTRAINT [PK_Department] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DepartmentManager]    Script Date: 17/01/2020 11:08:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DepartmentManager](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[StoreId] [int] NULL,
	[Discriminator] [nvarchar](50) NOT NULL,
	[Token] [nvarchar](200) NULL,
	[Role] [int] NULL,
	[PasswordHash] [varbinary](200) NOT NULL,
	[PasswordSalt] [varbinary](200) NOT NULL,
 CONSTRAINT [PK_DepartmentManager] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Owner]    Script Date: 17/01/2020 11:08:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Owner](
	[Id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Owner] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 17/01/2020 11:08:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Icon] [nvarchar](100) NOT NULL,
	[Price] [float] NOT NULL,
	[DepartmentId] [int] NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Store]    Script Date: 17/01/2020 11:08:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Store](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[OwnerId] [int] NULL,
	[AddressId] [int] NULL,
	[StoreOwnerId] [int] NULL,
 CONSTRAINT [PK_Store] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StoreOwner]    Script Date: 17/01/2020 11:08:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StoreOwner](
	[Id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_StoreOwner] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Department] FOREIGN KEY([DepartmentId])
REFERENCES [dbo].[Department] ([Id])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_Department]
GO
ALTER TABLE [dbo].[Store]  WITH CHECK ADD  CONSTRAINT [FK_Store_Address] FOREIGN KEY([Id])
REFERENCES [dbo].[Address] ([Id])
GO
ALTER TABLE [dbo].[Store] CHECK CONSTRAINT [FK_Store_Address]
GO
ALTER TABLE [dbo].[Store]  WITH CHECK ADD  CONSTRAINT [FK_Store_Owner] FOREIGN KEY([OwnerId])
REFERENCES [dbo].[Owner] ([Id])
GO
ALTER TABLE [dbo].[Store] CHECK CONSTRAINT [FK_Store_Owner]
GO
ALTER TABLE [dbo].[Store]  WITH CHECK ADD  CONSTRAINT [FK_Store_StoreOwner] FOREIGN KEY([Id])
REFERENCES [dbo].[StoreOwner] ([Id])
GO
ALTER TABLE [dbo].[Store] CHECK CONSTRAINT [FK_Store_StoreOwner]
GO
USE [master]
GO
ALTER DATABASE [IntegrationDatabase] SET  READ_WRITE 
GO
