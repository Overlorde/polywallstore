﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using PolyWallStoreApi.Data;
using PolyWallStoreApi.Helper;
using PolyWallStoreApi.Services;
using System;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using System.Globalization;
using System.Linq;
using NSwag;
using NSwag.Generation.Processors.Security;

namespace PolyWallStore
{
    /// <summary>
    /// Startup class.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Startup constructor
        /// </summary>
        /// <param name="configuration">The IConfiguration object</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services">The IServiceCollection object</param>
        public virtual void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddControllers();
            // configure strongly typed settings objects
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);
            services.AddAutoMapper(typeof(Startup));
            // configure jwt authentication
            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.Events = new JwtBearerEvents
                {
                    OnTokenValidated = context =>
                    {
                        var userService = context.HttpContext.RequestServices.GetRequiredService<IUserService>();
                        var userId = int.Parse(context.Principal.Identity.Name, CultureInfo.CurrentCulture);
                        var user = userService.GetDepartmentManagerById(userId); //Normally StoreOwner + Owner are include inside
                        if (user == null)
                        {
                            // return unauthorized if user no longer exists
                            context.Fail("Unauthorized");
                        }
                        return Task.CompletedTask;
                    }
                };
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });


            // configure DI for application services
            services.AddScoped<IUserService, UserService>();
            
            services.AddDbContext<PolyWallStoreApiContext>(options =>
                    options.UseSqlServer(Configuration.GetConnectionString("PolyWallStoreApiContext")));
            
            // Register the Swagger generator, defining 1 or more Swagger documents
            // Register the Swagger services
            services.AddOpenApiDocument(config =>
            {

                config.PostProcess = document =>
                {
                    document.Info.Version = "v1";
                    document.Info.Title = "PolyWallStore API";
                    document.Info.Description = "A simple ASP.NET Core web API";
                    document.Info.TermsOfService = "None";
                    document.Info.Contact = new NSwag.OpenApiContact
                    {
                        Name = "Alexandre Burnier-Framboret",
                        Email = string.Empty,
                        Url = "https://gitlab.com/Overlorde"
                    };
                    document.Info.License = new NSwag.OpenApiLicense
                    {
                        Name = "Not Under Licence",
                        Url = string.Empty
                    };
                };
                config.AddSecurity("JWT", Enumerable.Empty<string>(), new OpenApiSecurityScheme
                {
                    Type = OpenApiSecuritySchemeType.ApiKey,
                    Name = "Authorization",
                    In = OpenApiSecurityApiKeyLocation.Header,
                    Description = "Type into the textbox: Bearer {your JWT token}."
                });

                config.OperationProcessors.Add(
                    new AspNetCoreOperationSecurityScopeProcessor("JWT"));
                //      new OperationSecurityScopeProcessor("JWT"));
            });
        }



        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app">The IApplicationBuilder object</param>
        /// <param name="env">The IWebHostEnvironment object</param>
        public virtual void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (app != null)
            {
                app.UseRouting();

                if (env.IsDevelopment())
                {
                    app.UseDeveloperExceptionPage();
                }
                // Register the Swagger generator and the Swagger UI middlewares
                app.UseOpenApi();
                app.UseSwaggerUi3(settings =>
                {
                    settings.WithCredentials = true;
                });

                // global cors policy
                app.UseCors(x => x
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader());

                app.UseHttpsRedirection();

                app.UseAuthentication();
                app.UseAuthorization();

                app.UseEndpoints(endpoints =>
                {
                    endpoints.MapControllers();
                });
            }
            else
            {
                throw new ArgumentNullException(nameof(app));
            }
        }
    }
}
