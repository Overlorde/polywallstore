﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PolyWallStoreApi.Data;
using PolyWallStoreApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using NSwag.Annotations;
using Microsoft.AspNetCore.Authorization;
using PolyWallStoreApi.Helper;

namespace PolyWallStoreApi.Controllers
{
    /// <summary>
    /// The Product Controller
    /// </summary>
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [OpenApiTag("ProductsController", Description = "Product management")]
    public class ProductsController : ControllerBase
    {
        private readonly PolyWallStoreApiContext _context;

        public ProductsController(PolyWallStoreApiContext context)
        {
            _context = context;
        }

        // GET: api/Products
        /// <summary>
        /// Gets all Products
        /// </summary>
        /// <returns>The list of Products</returns>
        [HttpGet]
        [AuthorizeEnum(Role.DepartmentManager, Role.StoreOwner, Role.Owner)]
        public async Task<ActionResult<IEnumerable<Product>>> GetProduct()
        {
            return await _context.Product.ToListAsync().ConfigureAwait(true);
        }

        // GET: api/Products/5
        /// <summary>
        /// Get a Product
        /// </summary>
        /// <param name="id">The Id of the Product</param>
        /// <returns>A Product</returns>
        [HttpGet("{id}")]
        [AuthorizeEnum(Role.DepartmentManager, Role.StoreOwner, Role.Owner)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<Product>> GetProduct(int id)
        {
            var product = await _context.Product.FindAsync(id);

            if (product == null)
            {
                return NotFound();
            }

            return product;
        }

        // PUT: api/Products/5
        /// <summary>
        /// Modifies a Product
        /// </summary>
        /// <param name="id">The Id of the Product</param>
        /// <param name="product">The Product object</param>
        /// <returns></returns>
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        [AuthorizeEnum(Role.StoreOwner, Role.Owner)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> PutProduct(int id, Product product)
        {
            if (product != null)
            {
                if (id != product.Id)
                {
                    return BadRequest();
                }

                _context.Entry(product).State = EntityState.Modified;

                try
                {
                    await _context.SaveChangesAsync().ConfigureAwait(true);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return NoContent();
            }
            else
            {
                throw new ArgumentNullException(nameof(product));
            }
        }

        // POST: api/Products
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        /// <summary>
        /// Creates a Product
        /// </summary>
        /// <param name="product">The Product object</param>
        /// <returns></returns>
        [HttpPost]
        [AuthorizeEnum(Role.StoreOwner, Role.Owner)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<Product>> PostProduct(Product product)
        {
            if (product != null)
            {
                _context.Product.Add(product);
                await _context.SaveChangesAsync().ConfigureAwait(true);

                return CreatedAtAction("GetProduct", new { id = product.Id }, product);
            }
            else
            {
                throw new ArgumentNullException(nameof(product));
            }
        }

        // DELETE: api/Products/5
        /// <summary>
        /// Deletes a Product
        /// </summary>
        /// <param name="id">The Id of the Product</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [AuthorizeEnum(Role.StoreOwner, Role.Owner)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<Product>> DeleteProduct(int id)
        {
            var product = await _context.Product.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            _context.Product.Remove(product);
            await _context.SaveChangesAsync().ConfigureAwait(true);

            return product;
        }

        private bool ProductExists(int id)
        {
            return _context.Product.Any(e => e.Id == id);
        }
    }
}
