﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NSwag.Annotations;
using PolyWallStoreApi.Helper;
using PolyWallStoreApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PolyWallStoreApi.Data
{
    /// <summary>
    /// The Address Controller
    /// </summary>
    /// 
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [OpenApiTag("AddressesController", Description = "Addresses management")]
    public class AddressesController : ControllerBase
    {
        private readonly PolyWallStoreApiContext _context;

        public AddressesController(PolyWallStoreApiContext context)
        {
            _context = context;
        }

        // GET: api/Addresses
        /// <summary>
        /// Gets all Addresses
        /// </summary>
        /// <returns>The list of Addresses</returns>
        [AuthorizeEnum(Role.DepartmentManager, Role.StoreOwner, Role.Owner)]
        [HttpGet]
        [ApiConventionMethod(typeof(DefaultApiConventions),
                     nameof(DefaultApiConventions.Get))]
        public async Task<ActionResult<IEnumerable<Address>>> GetAddress()
        {
            return await _context.Address.ToListAsync().ConfigureAwait(true);
        }

        // GET: api/Addresses/5
        /// <summary>
        /// Get an Address
        /// </summary>
        /// <param name="id">The Id of the Address</param>
        /// <returns>An Address</returns>
        [AuthorizeEnum(Role.DepartmentManager, Role.StoreOwner, Role.Owner)]
        [HttpGet("{id}")]
        //[AuthorizeEnum(Role.DepartmentManager, Role.StoreOwner, Role.Owner)]
        [ApiConventionMethod(typeof(DefaultApiConventions),
                     nameof(DefaultApiConventions.Get))]
        public async Task<ActionResult<Address>> GetAddress(int id)
        {
            var address = await _context.Address.FindAsync(id);

            if (address == null)
            {
                return NotFound();
            }

            return address;
        }

        // PUT: api/Addresses/5
        /// <summary>
        /// Modify the Address
        /// </summary>
        /// <param name="id">The Id of the Address</param>
        /// <param name="address">The Address object</param>
        /// <returns></returns>
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [AuthorizeEnum(Role.Owner)]
        [HttpPut("{id}")]
        [ApiConventionMethod(typeof(DefaultApiConventions),
                     nameof(DefaultApiConventions.Put))]
        public async Task<IActionResult> PutAddress(int id, Address address)
        {
            if (address != null)
            {
                if (id != address.Id)
                {
                    return BadRequest();
                }

                _context.Entry(address).State = EntityState.Modified;

                try
                {
                    await _context.SaveChangesAsync().ConfigureAwait(true);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AddressExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return NoContent();
            }
            else
            {
                throw new ArgumentNullException(nameof(address));
            }
        }
        /// <summary>
        /// Creates an Address
        /// </summary>
        /// <param name="address">The Address object</param>
        /// <returns></returns>
        [AuthorizeEnum(Role.Owner)]
        [HttpPost]
        [ApiConventionMethod(typeof(DefaultApiConventions),
                     nameof(DefaultApiConventions.Post))]
        public async Task<ActionResult<Address>> PostAddress(Address address)
        {
            if (address != null)
            {
                _context.Address.Add(address);
                await _context.SaveChangesAsync().ConfigureAwait(true);

                return CreatedAtAction("GetAddress", new { id = address.Id }, address);
            }
            else
            {
                throw new ArgumentNullException(nameof(address));
            }
        }

        // DELETE: api/Addresses/5
        /// <summary>
        /// Deletes an Address
        /// </summary>
        /// <param name="id">The Id of the Address</param>
        /// <returns></returns>
        [AuthorizeEnum(Role.Owner)]
        [HttpDelete("{id}")]
        [ApiConventionMethod(typeof(DefaultApiConventions),
                     nameof(DefaultApiConventions.Delete))]
        public async Task<ActionResult<Address>> DeleteAddress(int id)
        {
            var address = await _context.Address.FindAsync(id);
            if (address == null)
            {
                return NotFound();
            }

            _context.Address.Remove(address);
            await _context.SaveChangesAsync().ConfigureAwait(true);

            return address;
        }

        private bool AddressExists(int id)
        {
            return _context.Address.Any(e => e.Id == id);
        }
    }
}
