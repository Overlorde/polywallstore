﻿using Microsoft.AspNetCore.Mvc;
using PolyWallStoreApi.Models;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using NSwag.Annotations;
using Microsoft.AspNetCore.Authorization;
using PolyWallStoreApi.Helper;
using PolyWallStoreApi.ExposeModels;
using PolyWallStoreApi.Services;
using AutoMapper;
using Microsoft.Extensions.Options;

namespace PolyWallStoreApi.Controllers
{
    /// <summary>
    /// The StoreOwner Controller
    /// </summary>
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [OpenApiTag("StoreOwnersController", Description = "StoreOwner management")]
    public class StoreOwnersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;

        public StoreOwnersController(IUserService userService, IMapper mapper, IOptions<AppSettings> appSettings)
        {
            _userService = userService;
            _mapper = mapper;
            if (appSettings != null)
            {
                _appSettings = appSettings.Value;
            }
            else
            {
                throw new ArgumentNullException(nameof(appSettings));
            }
        }

        // GET: api/StoreOwners
        /// <summary>
        /// Gets all StoreOwners
        /// </summary>
        /// <returns>The List of StoreOwners</returns>
        [HttpGet]
        [AuthorizeEnum(Role.DepartmentManager, Role.StoreOwner, Role.Owner)]
        public IActionResult GetStoreOwner()
        {
            var users = _userService.GetAllDepartmentManager();
            var model = _mapper.Map<IList<UserModel>>(users);
            return Ok(model);
        }

        // GET: api/StoreOwners/5
        /// <summary>
        /// Get a StoreOwner
        /// </summary>
        /// <param name="id">The Id of the StoreOwner</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [AuthorizeEnum(Role.DepartmentManager, Role.StoreOwner, Role.Owner)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public IActionResult GetStoreOwner(int id)
        {
            var user = _userService.GetStoreOwnerById(id);
            var model = _mapper.Map<UserModel>(user);
            return Ok(model);
        }

        // PUT: api/StoreOwners/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        /// <summary>
        /// Modifies a StoreOwner
        /// </summary>
        /// <param name="id">The Id of the StoreOwner</param>
        /// <param name="model">The StoreOwner object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [AuthorizeEnum(Role.StoreOwner, Role.Owner)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult PutStoreOwner(int id, [FromBody] UpdateStoreOwner model)
        {
            if (model != null)
            {
                // map model to entity and set id
                var user = _mapper.Map<StoreOwner>(model);
                user.Id = id;

                try
                {
                    // update user 
                    _userService.UpdateStoreOwner(user, model.Password);
                    return Ok();
                }
                catch (AppException ex)
                {
                    // return error message if there was an exception
                    return BadRequest(new { message = ex.Message });
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(model));
            }
        }

        // POST: api/StoreOwners
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        /// <summary>
        /// Creates a StoreOwner
        /// </summary>
        /// <param name="model">The StoreOwner object</param>
        /// <returns></returns>
        [HttpPost]
        [AuthorizeEnum(Role.StoreOwner, Role.Owner)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesDefaultResponseType]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult PostStoreOwner([FromBody] RegisterStoreOwner model)
        {

            // map model to entity
            var user = _mapper.Map<StoreOwner>(model);

            try
            {
                if (model != null)
                {
                    // create user
                    _userService.CreateStoreOwner(user, model.Password);
                    return Ok();
                }
                else
                {
                    throw new ArgumentNullException(nameof(model));
                }
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        // DELETE: api/StoreOwners/5
        /// <summary>
        /// Deletes a StoreOwner
        /// </summary>
        /// <param name="id">The Id of the StoreOwner</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [AuthorizeEnum(Role.StoreOwner, Role.Owner)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public IActionResult DeleteStoreOwner(int id)
        {
            _userService.DeleteStoreOwner(id);
            return Ok();
        }
    }
}
