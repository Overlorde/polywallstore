﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using PolyWallStore.Models;
using PolyWallStoreApi.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using NSwag.Annotations;
using Microsoft.AspNetCore.Authorization;
using PolyWallStoreApi.Helper;
using PolyWallStoreApi.Models;

namespace PolyWallStoreApi.Controllers
{
    /// <summary>
    /// The Store Controller
    /// </summary>
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [OpenApiTag("StoresController", Description = "Store management")]
    public class StoresController : ControllerBase
    {
        private readonly PolyWallStoreApiContext _context;

        [ActivatorUtilitiesConstructor]
        public StoresController(PolyWallStoreApiContext context)
        {
            _context = context;
        }

        // GET: api/Stores
        /// <summary>
        /// Gets all Stores
        /// </summary>
        /// <returns>The list of Stores</returns>
        [HttpGet]
        [AuthorizeEnum(Role.DepartmentManager, Role.StoreOwner, Role.Owner)]
        public async Task<ActionResult<IEnumerable<Store>>> GetStore()
        {
            return await _context.Store.ToListAsync().ConfigureAwait(true);
        }

        // GET: api/Stores/5
        /// <summary>
        /// Get a Store
        /// </summary>
        /// <param name="id">The Id of the Store</param>
        /// <returns>A Store</returns>
        [HttpGet("{id}")]
        [AuthorizeEnum(Role.DepartmentManager, Role.StoreOwner, Role.Owner)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<Store>> GetStore(int id)
        {
            var store = await _context.Store.FindAsync(id);

            if (store == null)
            {
                return NotFound();
            }

            return store;
        }

        // PUT: api/Stores/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        /// <summary>
        /// Modifies an existing Store
        /// </summary>
        /// <param name="id">The Id of the Store</param>
        /// <param name="store">The Store object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [AuthorizeEnum(Role.Owner)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> PutStore(int id,Store store)
        {
            if (store != null)
            {
                if (id != store.Id)
                {
                    return BadRequest();
                }
           

            _context.Entry(store).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync().ConfigureAwait(true);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StoreExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
                return NoContent();
            }
            else
            {
                throw new ArgumentNullException(nameof(store));
            }
        }

        // POST: api/Stores
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        /// <summary>
        /// Creates a Store
        /// </summary>
        /// <param name="store">The Store object</param>
        /// <returns></returns>
        [HttpPost]
        [AuthorizeEnum(Role.Owner)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<Store>> PostStore(Store store)
        {
            if (store != null)
            {
                _context.Store.Add(store);
                await _context.SaveChangesAsync().ConfigureAwait(true);

                return CreatedAtAction("GetStore", new { id = store.Id }, store);
            }
            else
            {
                throw new ArgumentNullException(nameof(store));
            }
        }

        // DELETE: api/Stores/5
        /// <summary>
        /// Deletes a Store
        /// </summary>
        /// <param name="id">The Id of the Store</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [AuthorizeEnum(Role.Owner)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<Store>> DeleteStore(int id)
        {
            var store = await _context.Store.FindAsync(id);
            if (store == null)
            {
                return NotFound();
            }

            _context.Store.Remove(store);
            await _context.SaveChangesAsync().ConfigureAwait(true);

            return store;
        }

        private bool StoreExists(int id)
        {
            return _context.Store.Any(e => e.Id == id);
        }

    }
}
