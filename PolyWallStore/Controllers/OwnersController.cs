﻿using Microsoft.AspNetCore.Mvc;
using PolyWallStoreApi.Models;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using NSwag.Annotations;
using Microsoft.AspNetCore.Authorization;
using PolyWallStoreApi.Helper;
using PolyWallStoreApi.ExposeModels;
using PolyWallStoreApi.Services;
using AutoMapper;
using Microsoft.Extensions.Options;

namespace PolyWallStoreApi.Controllers
{
    /// <summary>
    /// The Owner Controller
    /// </summary>
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [OpenApiTag("OwnersController", Description = "Owners management")]
    public class OwnersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;

        public OwnersController(IUserService userService, IMapper mapper, IOptions<AppSettings> appSettings)
        {
            _userService = userService;
            _mapper = mapper;
            if (appSettings != null)
            {
                _appSettings = appSettings.Value;
            }
            else
            {
                throw new ArgumentNullException(nameof(appSettings));
            }
        }

        // GET: api/Owners
        /// <summary>
        /// Gets all Owners
        /// </summary>
        /// <returns>The list of Owners</returns>
        [HttpGet]
        [AuthorizeEnum(Role.DepartmentManager, Role.StoreOwner, Role.Owner)]
        public IActionResult GetOwner()
        {
            var users = _userService.GetAllOwner();
            var model = _mapper.Map<IList<UserModel>>(users);
            return Ok(model);
        }

        // GET: api/Owners/5
        /// <summary>
        /// Get an Owner
        /// </summary>
        /// <param name="id">The Id of the Owner</param>
        /// <returns>A Owner</returns>
        [HttpGet("{id}")]
        [AuthorizeEnum(Role.DepartmentManager, Role.StoreOwner, Role.Owner)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public IActionResult GetOwner(int id)
        {
            var user = _userService.GetOwnerById(id);
            var model = _mapper.Map<UserModel>(user);
            return Ok(model);
        }

        // PUT: api/Owners/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        /// <summary>
        /// Modifies an Owner
        /// </summary>
        /// <param name="id">The Id of the Owner</param>
        /// <param name="model">The Owner object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [AuthorizeEnum(Role.Owner)] 
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult PutOwner(int id, [FromBody] UpdateOwner model)
        {
            if (model != null)
            {
                // map model to entity and set id
                var user = _mapper.Map<Owner>(model);
                user.Id = id;

                try
                {
                    // update user 
                    _userService.UpdateOwner(user, model.Password);
                    return Ok();
                }
                catch (AppException ex)
                {
                    // return error message if there was an exception
                    return BadRequest(new { message = ex.Message });
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(model));
            }
        }

        // POST: api/Owners
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        /// <summary>
        /// Creates an Owner
        /// </summary>
        /// <param name="model">The Owner object</param>
        /// <returns></returns>
        [HttpPost]
        [AuthorizeEnum(Role.Owner)]
        [ProducesResponseType(StatusCodes.Status201Created)] 
        [ProducesDefaultResponseType]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult PostOwner([FromBody] RegisterOwner model)
        {
            // map model to entity
            var user = _mapper.Map<Owner>(model);

            try
            {
                if (model != null)
                {
                    // create user
                    _userService.CreateOwner(user, model.Password);
                    return Ok(); //return Ok(user) instead ?
                }
                else
                {
                    throw new ArgumentNullException(nameof(model));
                }
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        // DELETE: api/Owners/5
        /// <summary>
        /// Deletes an Owner
        /// </summary>
        /// <param name="id">The Id of the Owner</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [AuthorizeEnum(Role.Owner)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public IActionResult DeleteOwner(int id)
        {
            _userService.DeleteOwner(id);
            return Ok();
        }
    }
}
