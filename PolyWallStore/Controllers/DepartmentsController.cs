﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PolyWallStoreApi.Data;
using PolyWallStoreApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using NSwag.Annotations;
using Microsoft.AspNetCore.Authorization;
using PolyWallStoreApi.Helper;

namespace PolyWallStoreApi.Controllers
{
    /// <summary>
    /// The Department Controller
    /// </summary>
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [OpenApiTag("DepartmentsController", Description = "Departments management")]
    public class DepartmentsController : ControllerBase
    {
        private readonly PolyWallStoreApiContext _context;

        public DepartmentsController(PolyWallStoreApiContext context)
        {
            _context = context;
        }

        // GET: api/Departments
        /// <summary>
        /// Gets all Departments
        /// </summary>
        /// <returns>The list of Departments</returns>
        [HttpGet]
        [AuthorizeEnum(Role.DepartmentManager, Role.StoreOwner, Role.Owner)]
        public async Task<ActionResult<IEnumerable<Department>>> GetDepartment()
        {
            return await _context.Department.ToListAsync().ConfigureAwait(true);
        }

        // GET: api/Departments/5
        /// <summary>
        /// Get a Department
        /// </summary>
        /// <param name="id">The Id of the Department</param>
        /// <returns>A Department</returns>
        [HttpGet("{id}")]
        [AuthorizeEnum(Role.DepartmentManager, Role.StoreOwner, Role.Owner)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<Department>> GetDepartment(int id)
        {
            var department = await _context.Department.FindAsync(id);

            if (department == null)
            {
                return NotFound();
            }

            return department;
        }

        // PUT: api/Departments/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        /// <summary>
        /// Modifies a Department
        /// </summary>
        /// <param name="id">The Id of the Department</param>
        /// <param name="department">The Address object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [AuthorizeEnum(Role.StoreOwner, Role.Owner)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> PutDepartment(int id, Department department)
        {
            if (department != null)
            {
                if (id != department.Id)
                {
                    return BadRequest();
                }

                _context.Entry(department).State = EntityState.Modified;

                try
                {
                    await _context.SaveChangesAsync().ConfigureAwait(true);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DepartmentExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return NoContent();
            }
            else
            {
                throw new ArgumentNullException(nameof(department));
            }
        }

        // POST: api/Departments
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        /// <summary>
        /// Creates a Department
        /// </summary>
        /// <param name="department">The Department object</param>
        /// <returns></returns>
        [HttpPost]
        [AuthorizeEnum(Role.StoreOwner, Role.Owner)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<Department>> PostDepartment(Department department)
        {
            if (department != null)
            {
                _context.Department.Add(department);
                await _context.SaveChangesAsync().ConfigureAwait(true);

                return CreatedAtAction("GetDepartment", new { id = department.Id }, department);
            }
            else
            {
                throw new ArgumentNullException(nameof(department));
            }
        }

        // DELETE: api/Departments/5
        /// <summary>
        /// Deletes a Department
        /// </summary>
        /// <param name="id">The Id of the Department</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [AuthorizeEnum(Role.StoreOwner, Role.Owner)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<Department>> DeleteDepartment(int id)
        {
            var department = await _context.Department.FindAsync(id);
            if (department == null)
            {
                return NotFound();
            }

            _context.Department.Remove(department);
            await _context.SaveChangesAsync().ConfigureAwait(true);

            return department;
        }

        private bool DepartmentExists(int id)
        {
            return _context.Department.Any(e => e.Id == id);
        }
    }
}
