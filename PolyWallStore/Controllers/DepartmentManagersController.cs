﻿using Microsoft.AspNetCore.Mvc;
using PolyWallStoreApi.Models;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using NSwag.Annotations;
using Microsoft.AspNetCore.Authorization;
using PolyWallStoreApi.Services;
using AutoMapper;
using PolyWallStoreApi.Helper;
using Microsoft.Extensions.Options;
using System.Text;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Globalization;

namespace PolyWallStoreApi.Controllers
{
    /// <summary>
    /// The DepartmentManager Controller
    /// </summary>
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [OpenApiTag("DepartmentManagerController", Description = "DepartmentManager management")]
    public class DepartmentManagersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;

        public DepartmentManagersController(IUserService userService, IMapper mapper, IOptions<AppSettings> appSettings)
        {
            _userService = userService;
            _mapper = mapper;
            if (appSettings != null)
            {
                _appSettings = appSettings.Value;
            }
            else
            {
                throw new ArgumentNullException(nameof(appSettings));
            }
        }
        /// <summary>
        /// Allow the user to be identified
        /// </summary>
        /// <param name="model">The model object</param>
        /// <returns>The Authentification</returns>
        [AllowAnonymous]
        [HttpPost("authenticate")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesDefaultResponseType]
        public IActionResult Authenticate([FromBody]AuthenticateModel model)
        {
            if (model != null)
            {
                var user = _userService.Authenticate(model.Email, model.Password);

                if (user == null)
                {
                    return BadRequest(new { message = "Username or password is incorrect" });
                }
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                    new Claim(ClaimTypes.Name, user.Id.ToString("G", CultureInfo.CurrentCulture)),
                    new Claim(ClaimTypes.Role, user.Role.ToString())
                    }),
                    Expires = DateTime.UtcNow.AddDays(1),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);
                var tokenString = tokenHandler.WriteToken(token);

                // return basic user info and authentication token
                return Ok(new
                {
                    Id = user.Id,
                    Email = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Role = user.Role,
                    Token = tokenString
                });
            }
            else
            {
                throw new ArgumentNullException(nameof(model));
            }
        }
        /// <summary>
        /// Allows the user to be registered in the system
        /// </summary>
        /// <param name="model">The Model object</param>
        /// <returns></returns>
        [AuthorizeEnum(Role.StoreOwner, Role.Owner)] //AN OWNER ACCOUNT MUST BE INSERTED IN DATABASE TO CONSTRUCT FULL SYSTEM
        [HttpPost("register")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesDefaultResponseType]
        public IActionResult Register([FromBody]RegisterDepartmentManager model)
        {
            // map model to entity
            var user = _mapper.Map<DepartmentManager>(model);

            try
            {
                if (model != null)
                {
                    // create user
                    _userService.CreateDepartmentManager(user, model.Password);
                    return Ok();
                }
                else
                {
                    throw new ArgumentNullException(nameof(model));
                }
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
        /// <summary>
        /// Gets all the DepartmentManagers
        /// </summary>
        /// <returns>The list of DepartmentManagers</returns>
        [HttpGet]
        [AuthorizeEnum(Role.DepartmentManager, Role.StoreOwner, Role.Owner)]
        public IActionResult GetDepartmentManager()
        {
            var users = _userService.GetAllDepartmentManager();
            var model = _mapper.Map<IList<UserModel>>(users);
            return Ok(model);
        }
        /// <summary>
        /// Gets a DepartmentManager
        /// </summary>
        /// <param name="id">The Id of the DepartmentManager</param>
        /// <returns>A DepartmentManager</returns>
        [HttpGet("{id}")]
        [AuthorizeEnum(Role.StoreOwner, Role.Owner)]
        public IActionResult GetDepartmentManager(int id)
        {
            var user = _userService.GetDepartmentManagerById(id);
            var model = _mapper.Map<UserModel>(user);
            return Ok(model);
        }
        /// <summary>
        /// Modifies a DepartmentManager
        /// </summary>
        /// <param name="id">The Id of the DepartmentManager</param>
        /// <param name="model">The model obejct</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [AuthorizeEnum(Role.StoreOwner, Role.Owner)] //VOIR REDUIRE UPDATE UNIQUEMENT SUR LE MAGASIN POUR STOREOWNER
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesDefaultResponseType]
        public IActionResult PutDepartmentManager(int id, [FromBody]UpdateDepartmentManager model)
        {
            if (model != null)
            {
                // map model to entity and set id
                var user = _mapper.Map<DepartmentManager>(model);
                user.Id = id;

                try
                {
                    // update user 
                    _userService.UpdateDepartmentManager(user, model.Password);
                    return Ok();
                }
                catch (AppException ex)
                {
                    // return error message if there was an exception
                    return BadRequest(new { message = ex.Message });
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(model));
            }
        }
        /// <summary>
        /// Deletes a DepartmentManager
        /// </summary>
        /// <param name="id">The Id of the DepartmentManager</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [AuthorizeEnum(Role.StoreOwner, Role.Owner)]
        public IActionResult DeleteDepartmentManager(int id)
        {
            _userService.DeleteDepartmentManager(id);
            return Ok();
        }
    }
}
