﻿using System.ComponentModel.DataAnnotations;

namespace PolyWallStoreApi.Models
{
    /// <summary>
    /// Product class.
    /// </summary>
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }
        public double Price { get; set; }
        public Department Department { get; set; }
    }
}
