﻿using PolyWallStore.Models;
using System.Collections.Generic;

namespace PolyWallStoreApi.Models
{
    /// <summary>
    /// Owner class.
    /// </summary>
    public class Owner : DepartmentManager
    {
        public Owner()
        {

        }
        public Owner(ICollection<Store> Stores = null) {
            this.Stores = Stores;
        }
        public ICollection<Store> Stores { get; }
    }
}
