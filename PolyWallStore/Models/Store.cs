﻿using PolyWallStoreApi.Models;
using System.Collections.Generic;

namespace PolyWallStore.Models
{
    /// <summary>
    /// Store class.
    /// </summary>
    public class Store
    {
        public Store()
        {
            DepartmentManagers = new List<DepartmentManager>();
            Departments = new List<Department>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public int? AddressId { get; set; }
        public Address Address { get; set; }
        public ICollection<DepartmentManager> DepartmentManagers { get; }
        public ICollection<Department> Departments { get; }
        public Owner Owner { get; set; }
        public int? StoreOwnerId { get; set; }
        public StoreOwner StoreOwner { get; set; }

    }
}
