﻿using PolyWallStore.Models;

namespace PolyWallStoreApi.Models
{
    /// <summary>
    /// StoreOwner class.
    /// </summary>
    public class StoreOwner : DepartmentManager
    {
        public StoreOwner()
        {

        }
        public new Store Store { get; set; }
    }
}
