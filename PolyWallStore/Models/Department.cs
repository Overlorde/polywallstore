﻿using PolyWallStore.Models;
using System.Collections.Generic;

namespace PolyWallStoreApi.Models
{
    /// <summary>
    /// Department class.
    /// </summary>
    public class Department
    {
        public Department()
        {
            Products = new List<Product>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public Store Store { get; set; }

        public ICollection<Product> Products { get; }
        public int? DepartmentManagerId { get; set; }
        public DepartmentManager DepartmentManager { get; set; }
    }
}
