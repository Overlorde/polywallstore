﻿using PolyWallStore.Models;

namespace PolyWallStoreApi.Models
{
    public enum Role
    {
        DepartmentManager,
        StoreOwner,
        Owner
    }
    /// <summary>
    /// DepartmentManager class.
    /// </summary>
    public class DepartmentManager
    {
        public DepartmentManager()
        {

        }
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public byte[] PasswordHash { set; get; }
        //public byte[] GetPasswordHash()
        //{
        //    // Need to return a clone of the array so that consumers            
        //    // of this library cannot change its contents            
        //    return (byte[])PasswordHash.Clone();
        //}
        public byte[] PasswordSalt { get; set; }
        //public byte[] GetPasswordSalt()
        //{
        //    // Need to return a clone of the array so that consumers            
        //    // of this library cannot change its contents            
        //    return (byte[])PasswordSalt.Clone();
        //}
        public Store Store { get; set; }

        public Department Department { get; set; }
        public Role Role { get; set; }
        public string Token { get; set; }
    }
}
