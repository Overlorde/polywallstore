﻿using PolyWallStore.Models;

namespace PolyWallStoreApi.Models
{
    /// <summary>
    /// Address class.
    /// </summary>
    public class Address
    {

        public int Id { get; set; }
        public string City { get; set; }

        public string Street { get; set; }

        public int ZipCode { get; set; }
        public Store Store { get; set; }
    }
}
