﻿using PolyWallStore.Models;
using PolyWallStoreApi.Models;
using System.ComponentModel.DataAnnotations;

namespace PolyWallStoreApi.ExposeModels
{
    public class RegisterStoreOwner
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        public Store Store { get; set; }
        [Required]
        public Role Role { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
