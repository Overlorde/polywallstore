﻿using PolyWallStore.Models;
using PolyWallStoreApi.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PolyWallStoreApi.ExposeModels
{
    public class RegisterOwner
    {
        public RegisterOwner()
        {
        }
        public RegisterOwner(List<Store> Stores)
        {
            this.Stores = Stores;
        }
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        public List<Store> Stores { get; }
        [Required]
        public Role Role { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
