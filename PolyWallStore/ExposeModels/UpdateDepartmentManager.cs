﻿using PolyWallStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PolyWallStoreApi.Models
{
    public class UpdateDepartmentManager
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }
        public string Email { get; set; }
        public Store Store { get; set; }
        public Department Department { get; set; }
        public Role Role { get; set; }

        public string Password { get; set; }
    }
}
