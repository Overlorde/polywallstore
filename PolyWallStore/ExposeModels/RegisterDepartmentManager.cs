﻿using PolyWallStore.Models;
using System.ComponentModel.DataAnnotations;

namespace PolyWallStoreApi.Models
{
    public class RegisterDepartmentManager
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        public Store Store { get; set; }
        public Department Department { get; set; }
        [Required]
        public Role Role { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
