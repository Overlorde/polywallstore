using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace PolyWallStore
{
    /// <summary>
    /// Program class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// Main method used to launch the program
        /// </summary>
        /// <param name="args">The args options</param>
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }
        /// <summary>
        /// Allow to load project parameters
        /// </summary>
        /// <param name="args">The args options</param>
        /// <returns>The IHostBuilder</returns>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
