﻿using System;

namespace PolyWallStoreApi.Helper
{
    /// <summary>
    /// Allows to throw personalized exceptions
    /// </summary>
    public class AppException : Exception
    {
        public AppException() : base() { }

        public AppException(string message) : base(message) { }

        public AppException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
