﻿using AutoMapper;
using PolyWallStoreApi.ExposeModels;
using PolyWallStoreApi.Models;

namespace PolyWallStoreApi.Helper
{
    /// <summary>
    /// Allows to Map the user classes with the exposed models to client (hides passwordSalt for instance)
    /// </summary>
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<DepartmentManager, UserModel>();
            CreateMap<RegisterDepartmentManager, DepartmentManager>();
            CreateMap<UpdateDepartmentManager, DepartmentManager>();
            CreateMap<StoreOwner, UserModel>();
            CreateMap<RegisterStoreOwner, StoreOwner>();
            CreateMap<UpdateStoreOwner, StoreOwner>();
            CreateMap<Owner, UserModel>();
            CreateMap<RegisterOwner, Owner>();
            CreateMap<UpdateOwner, Owner>();
        }
    }
}
