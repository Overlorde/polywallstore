﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Globalization;
using System.Linq;

namespace PolyWallStoreApi.Helper
{
    /// <summary>
    /// Allows to use string enum for Role in Authorize
    /// </summary>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
        public class AuthorizeEnumAttribute : AuthorizeAttribute
        {
            public AuthorizeEnumAttribute(params object[] roles)
            {
                if (roles.Any(r => r.GetType().BaseType != typeof(Enum)))
                    throw new ArgumentException(string.Format(CultureInfo.CurrentCulture,"{0} is not an existing role", roles));

                this.Roles = string.Join(",", roles.Select(r => Enum.GetName(r.GetType(), r)));
            }
        }
}
