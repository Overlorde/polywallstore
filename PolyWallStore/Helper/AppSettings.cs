﻿namespace PolyWallStoreApi.Helper
{
    /// <summary>
    /// Allows to get the secret which can authentify the token
    /// </summary>
    public class AppSettings
    {
        public string Secret { get; set; }
    }
}
