﻿using Microsoft.EntityFrameworkCore;
using PolyWallStore.Models;
using System;

namespace PolyWallStoreApi.Data
{
    /// <summary>
    /// PolyWallStoreApiContext class.
    /// </summary>
    public class PolyWallStoreApiContext : DbContext
    {
        /// <summary>
        /// PolyWallStoreApiContext constructor
        /// </summary>
        /// <param name="options">The DBContextOptions object</param>
        public PolyWallStoreApiContext(DbContextOptions<PolyWallStoreApiContext> options)
            : base(options)
        {

        }
        /// <summary>
        /// Allow to configure the EF database
        /// </summary>
        /// <param name="optionsBuilder">The optionBuilder object</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder != null)
            {
                if (!optionsBuilder.IsConfigured)
                {
                    optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=EFProviders.InMemory;Trusted_Connection=True;ConnectRetryCount=0");
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(optionsBuilder));
            }
        }
        public virtual DbSet<Store> Store { get; set; }
        public DbSet<Models.Address> Address { get; set; }
        public DbSet<Models.Department> Department { get; set; }
        public DbSet<Models.Owner> Owner { get; set; }
        public DbSet<Models.StoreOwner> StoreOwner { get; set; }
        public DbSet<Models.Product> Product { get; set; }
        public DbSet<Models.DepartmentManager> DepartmentManager { get; set; }
    }
}
