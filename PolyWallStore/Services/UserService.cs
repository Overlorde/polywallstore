﻿using PolyWallStoreApi.Data;
using PolyWallStoreApi.Helper;
using PolyWallStoreApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PolyWallStoreApi.Services
{
    /// <summary>
    /// Interface which allows to redefine the entity framework methods
    /// </summary>
    public interface IUserService
    {
        DepartmentManager Authenticate(string username, string password);
        IEnumerable<DepartmentManager> GetAllDepartmentManager();
        DepartmentManager GetDepartmentManagerById(int id);
        DepartmentManager CreateDepartmentManager(DepartmentManager user, string password);
        void UpdateDepartmentManager(DepartmentManager user, string password = null);
        void DeleteDepartmentManager(int id);
        IEnumerable<StoreOwner> GetAllStoreOwner();
        StoreOwner GetStoreOwnerById(int id);
        StoreOwner CreateStoreOwner(StoreOwner user, string password);
        void UpdateStoreOwner(StoreOwner user, string password = null);
        void DeleteStoreOwner(int id);
        IEnumerable<Owner> GetAllOwner();
        Owner GetOwnerById(int id);
        Owner CreateOwner(Owner user, string password);
        void UpdateOwner(Owner user, string password = null);
        void DeleteOwner(int id);
    }

    public class UserService : IUserService
    {
        // The repository which contains the Database
        private readonly PolyWallStoreApiContext _context;

        public UserService(PolyWallStoreApiContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Allows to authenticate the user
        /// </summary>
        /// <param name="username">User's Email</param>
        /// <param name="password">User's password</param>
        /// <returns></returns>
        public DepartmentManager Authenticate(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                return null;
            }

            var user = _context.DepartmentManager.SingleOrDefault(x => x.Email == username);

            // check if username exists
            if (user == null)
            {
                return null;
            }

            // check if password is correct
            if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
            {
                return null;
            }

            // authentication successful
            return user;
        }
        /// <summary>
        /// Allows to get All the DepartmentManager
        /// </summary>
        /// <returns>The DepartmentManagers</returns>
        public IEnumerable<DepartmentManager> GetAllDepartmentManager()
        {
            return _context.DepartmentManager;
        }
        /// <summary>
        /// Allows to get a DepartmentManager
        /// </summary>
        /// <param name="id">The Id of the DepartmentManager</param>
        /// <returns>A DepartmentManager</returns>
        public DepartmentManager GetDepartmentManagerById(int id)
        {
            return _context.DepartmentManager.Find(id);
        }
        /// <summary>
        /// Allows to create a DepartmentManager
        /// </summary>
        /// <param name="user">DepartmentManager's Email</param>
        /// <param name="password">DepartmentManager's password</param>
        /// <returns></returns>
        public DepartmentManager CreateDepartmentManager(DepartmentManager user, string password)
        {
            // validation
            if (user != null)
            {
                if (string.IsNullOrWhiteSpace(password))
                    throw new AppException(password);

                if (_context.DepartmentManager.Any(x => x.Email == user.Email))
                    if (user != null)
                    {
                        throw new AppException("Email \"" + user.Email + "\" is already taken");
                    }
                    else
                    {
                        throw new ArgumentNullException(nameof(user));
                    }

                CreatePasswordHash(password, out byte[] passwordHash, out byte[] passwordSalt);
                DepartmentManager userToInsert = new DepartmentManager
                {
                    Id = user.Id,
                    Department = user.Department,
                    Email = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Role = user.Role,
                    Store = user.Store,
                    Token = user.Token,
                    PasswordHash = passwordHash,
                    PasswordSalt = passwordSalt
                };
                _context.DepartmentManager.Add(userToInsert);
                _context.SaveChanges();

                return userToInsert;
            }
            else
            {
                throw new ArgumentNullException(nameof(user));
            }
        }
        /// <summary>
        /// Allows to update the informations about a DepartmentManager
        /// </summary>
        /// <param name="userParam">DepartmentManager's Email</param>
        /// <param name="password">DepartmentManager's Password</param>
        public void UpdateDepartmentManager(DepartmentManager userParam, string password = null)
        {
            if (userParam != null)
            {
                var user = _context.DepartmentManager.Find(userParam.Id);

                if (user == null)
                    throw new AppException(null);

                // update username if it has changed
                if (!string.IsNullOrWhiteSpace(userParam.Email) && userParam.Email != user.Email)
                {
                    // throw error if the new username is already taken
                    if (_context.DepartmentManager.Any(x => x.Email == userParam.Email))
                        throw new AppException("Email " + userParam.Email + " is already taken");

                    user.Email = userParam.Email;
                }

                // update user properties if provided
                if (!string.IsNullOrWhiteSpace(userParam.FirstName))
                    user.FirstName = userParam.FirstName;

                if (!string.IsNullOrWhiteSpace(userParam.LastName))
                    user.LastName = userParam.LastName;
                if (userParam.Store != null)
                    user.Store = userParam.Store;
                user.Role = userParam.Role;
                if (userParam.Department != null)
                    user.Department = userParam.Department;
                // update password if provided
                if (!string.IsNullOrWhiteSpace(password))
                {
                    CreatePasswordHash(password, out byte[] passwordHash, out byte[] passwordSalt); //Call it in put to change passwordhash + salt
                    DepartmentManager userToInsert = new DepartmentManager
                    {
                        Id = user.Id,
                        Department = user.Department,
                        Email = user.Email,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Role = user.Role,
                        Store = user.Store,
                        PasswordHash = passwordHash,
                        PasswordSalt = passwordSalt
                    };
                    _context.DepartmentManager.Update(userToInsert);

                }
                else
                {
                    _context.DepartmentManager.Update(user);
                }

                _context.SaveChanges();
            }
            else
            {
                throw new ArgumentNullException(nameof(userParam));
            }
        }
        /// <summary>
        /// Allows to delete a DepartmentManager
        /// </summary>
        /// <param name="id">The Id of the DepartmentManager</param>
        public void DeleteDepartmentManager(int id)
        {
            var user = _context.DepartmentManager.Find(id);
            if (user != null)
            {
                _context.DepartmentManager.Remove(user);
                _context.SaveChanges();
            }
        }

        // private helper methods
        /// <summary>
        /// Allows to create an encrypted password with hash + salt
        /// </summary>
        /// <param name="password">User's Password</param>
        /// <param name="passwordHash">User's PasswordHash</param>
        /// <param name="passwordSalt">User's PasswordSalt</param>
        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null) { throw new ArgumentNullException(nameof(password)); }
            if (string.IsNullOrWhiteSpace(password)) { throw new ArgumentException(password); }

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
            }
        }
        /// <summary>
        /// Allows to verify if the given password match with the hash + salt registered in Database
        /// </summary>
        /// <param name="password">User's Password</param>
        /// <param name="storedHash">User's PasswordHash</param>
        /// <param name="storedSalt">User's PasswordSalt</param>
        /// <returns></returns>
        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null) { throw new ArgumentNullException(nameof(password)); }
            if (string.IsNullOrWhiteSpace(password)) { throw new ArgumentException(password); }
            if (storedHash.Length != 64) { throw new ArgumentException(storedHash.ToString()); }
            if (storedSalt.Length != 128) { throw new ArgumentException(storedSalt.ToString()); }

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Allows to get all the StoreOwners
        /// </summary>
        /// <returns>The StoreOwners</returns>
        public IEnumerable<StoreOwner> GetAllStoreOwner()
        {
            return _context.StoreOwner;
        }
        /// <summary>
        /// Allows to get a StoreOwner
        /// </summary>
        /// <param name="id">StoreOwner's Id</param>
        /// <returns>A StoreOwner</returns>
        public StoreOwner GetStoreOwnerById(int id)
        {
            return _context.StoreOwner.Find(id);
        }
        /// <summary>
        /// Allows to create a StoreOwner
        /// </summary>
        /// <param name="user">StoreOwner's Email</param>
        /// <param name="password">StoreOwner's Password</param>
        /// <returns>The StoreOwner created</returns>
        public StoreOwner CreateStoreOwner(StoreOwner user, string password)
        {
            // validation
            if (user != null)
            {
                if (string.IsNullOrWhiteSpace(password))
                    throw new AppException(password);

                if (_context.StoreOwner.Any(x => x.Email == user.Email))
                    if (user != null)
                    {
                        throw new AppException("Email \"" + user.Email + "\" is already taken");
                    }
                    else
                    {
                        throw new ArgumentNullException(nameof(user));
                    }

                CreatePasswordHash(password, out byte[] passwordHash, out byte[] passwordSalt);

                StoreOwner userToInsert = new StoreOwner
                {
                    Id = user.Id,
                    Department = user.Department,
                    Email = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Role = user.Role,
                    Store = user.Store,
                    Token = user.Token,
                    PasswordHash = passwordHash,
                    PasswordSalt = passwordSalt
                };
                _context.StoreOwner.Add(userToInsert);
                _context.SaveChanges();

                return userToInsert;
            }
            else
            {
                throw new ArgumentNullException(nameof(user));
            }
        }
        /// <summary>
        /// Allows to update the informations about a StoreOwner
        /// </summary>
        /// <param name="userParam">StoreOwner's Email</param>
        /// <param name="password">StoreOwner's Password</param>
        public void UpdateStoreOwner(StoreOwner userParam, string password = null)
        {
            if (userParam != null)
            {
                var user = _context.StoreOwner.Find(userParam.Id);

                if (user == null)
                    throw new AppException(null);

                // update username if it has changed
                if (!string.IsNullOrWhiteSpace(userParam.Email) && userParam.Email != user.Email)
                {
                    // throw error if the new username is already taken
                    if (_context.DepartmentManager.Any(x => x.Email == userParam.Email))
                        throw new AppException("Email " + userParam.Email + " is already taken");

                    user.Email = userParam.Email;
                }

                // update user properties if provided
                if (!string.IsNullOrWhiteSpace(userParam.FirstName))
                    user.FirstName = userParam.FirstName;

                if (!string.IsNullOrWhiteSpace(userParam.LastName))
                    user.LastName = userParam.LastName;
                if (userParam.Store != null)
                    user.Store = userParam.Store;

                user.Role = userParam.Role;
                // update password if provided
                if (!string.IsNullOrWhiteSpace(password))
                {
                    CreatePasswordHash(password, out byte[] passwordHash, out byte[] passwordSalt);
                    StoreOwner userToInsert = new StoreOwner
                    {
                        Id = user.Id,
                        Department = user.Department,
                        Email = user.Email,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Role = user.Role,
                        Store = user.Store,
                        PasswordHash = passwordHash,
                        PasswordSalt = passwordSalt
                    };
                    _context.StoreOwner.Update(userToInsert);

                }
                else
                {
                    _context.StoreOwner.Update(user);
                }

                _context.SaveChanges();
            }
            else
            {
                throw new ArgumentNullException(nameof(userParam));
            }
        }
        /// <summary>
        /// Allows to delete a StoreOwner
        /// </summary>
        /// <param name="id">StoreOwner's Id</param>
        public void DeleteStoreOwner(int id)
        {
            var user = _context.StoreOwner.Find(id);
            if (user != null)
            {
                _context.StoreOwner.Remove(user);
                _context.SaveChanges();
            }
        }
        /// <summary>
        /// Allows to get all Owners
        /// </summary>
        /// <returns>The Owners</returns>
        public IEnumerable<Owner> GetAllOwner()
        {
            return _context.Owner;
        }
        /// <summary>
        /// Allows to get a Owners
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A Owner</returns>
        public Owner GetOwnerById(int id)
        {
            return _context.Owner.Find(id);
        }
        /// <summary>
        /// Allows to create a Owner
        /// </summary>
        /// <param name="user">Owner's Email</param>
        /// <param name="password">Owner's Password</param>
        /// <returns>The Owner created</returns>
        public Owner CreateOwner(Owner user, string password)
        {
            // validation
            if (user != null)
            {
                if (string.IsNullOrWhiteSpace(password))
                    throw new AppException(password);

                if (_context.DepartmentManager.Any(x => x.Email == user.Email))
                    if (user != null)
                    {
                        throw new AppException("Email \"" + user.Email + "\" is already taken");
                    }
                    else
                    {
                        throw new ArgumentNullException(nameof(user));
                    }

                CreatePasswordHash(password, out byte[] passwordHash, out byte[] passwordSalt);
                Owner userToInsert = new Owner(user.Stores)
                {
                    Id = user.Id,
                    Department = user.Department,
                    Email = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Role = user.Role,
                    Store = user.Store,
                    Token = user.Token,
                    PasswordHash = passwordHash,
                    PasswordSalt = passwordSalt
                };
                _context.Owner.Add(userToInsert);
                _context.SaveChanges();

                return userToInsert;
            }
            else
            {
                throw new ArgumentNullException(nameof(user));
            }
        }
        /// <summary>
        /// Allows to update the informations about a Owner
        /// </summary>
        /// <param name="userParam">Owner's Email</param>
        /// <param name="password">Owner's Password</param>
        public void UpdateOwner(Owner userParam, string password = null)
        {
            if (userParam != null)
            {
                var user = _context.Owner.Find(userParam.Id);

                if (user == null)
                    throw new AppException(null);

                // update username if it has changed
                if (!string.IsNullOrWhiteSpace(userParam.Email) && userParam.Email != user.Email)
                {
                    // throw error if the new username is already taken
                    if (_context.Owner.Any(x => x.Email == userParam.Email))
                        throw new AppException("Email " + userParam.Email + " is already taken");

                    user.Email = userParam.Email;
                }

                // update user properties if provided
                if (!string.IsNullOrWhiteSpace(userParam.FirstName))
                    user.FirstName = userParam.FirstName;

                if (!string.IsNullOrWhiteSpace(userParam.LastName))
                    user.LastName = userParam.LastName;
                if (userParam.Stores != null)
                    user.Store = userParam.Store;

                user.Role = userParam.Role;
                // update password if provided
                if (!string.IsNullOrWhiteSpace(password) || userParam.Stores != null)
                {
                    CreatePasswordHash(password, out byte[] passwordHash, out byte[] passwordSalt); //Call it in put to change passwordhash + salt
                    Owner userToInsert = new Owner(userParam.Stores)
                    {
                        Id = user.Id,
                        Department = user.Department,
                        Email = user.Email,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Role = user.Role,
                        Store = user.Store,
                        PasswordHash = passwordHash,
                        PasswordSalt = passwordSalt
                    };
                    _context.Owner.Update(userToInsert);

                }
                else
                {
                    _context.Owner.Update(user);
                }

                _context.SaveChanges();
            }
            else
            {
                throw new ArgumentNullException(nameof(userParam));
            }
        }
        /// <summary>
        /// Allows to delete a Owner
        /// </summary>
        /// <param name="id">Owner's Id</param>
        public void DeleteOwner(int id)
        {
            var user = _context.Owner.Find(id);
            if (user != null)
            {
                _context.Owner.Remove(user);
                _context.SaveChanges();
            }
        }
    }
}
