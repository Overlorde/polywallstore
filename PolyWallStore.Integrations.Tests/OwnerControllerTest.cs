﻿using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PolyWallStore;
using PolyWallStore.IntegrationTests;
using System.IO;
using System.Threading.Tasks;
using Xunit;

namespace PolyWallStore.IntegrationTests
{
    public class OwnerControllerTest : IClassFixture<MediaGalleryFactory<FakeStartup>>
    {
        private readonly WebApplicationFactory<FakeStartup> _factory;

        public OwnerControllerTest(MediaGalleryFactory<FakeStartup> factory)
        {
            var projectDir = Directory.GetCurrentDirectory();
            var configPath = Path.Combine(projectDir, "appsettings.json");

            _factory = factory.WithWebHostBuilder(builder =>
            {
                builder.UseSolutionRelativeContentRoot("PolyWallStore.Integrations.Tests");

                builder.ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.AddJsonFile(configPath);
                });

                builder.ConfigureTestServices(services =>
                {
                    services.AddMvc().AddApplicationPart(typeof(Startup).Assembly);
                });
            });
        }

        [Theory]
        [InlineData("/api/Owners")]
        public async Task Get_All_Owners_Should_Be_Unauthorized(string url)
        {
            // Arrange
            var client = _factory.CreateClient();

            // Act
            var response = await client.GetAsync(url);

            // Assert
            Assert.Equal(System.Net.HttpStatusCode.Unauthorized, response.StatusCode);
        }
        [Theory]
        [InlineData("/api/Owners")]
        public async Task Get_All_Owners_Should_Be_Authorized(string url)
        {
            // Arrange
            var client = _factory.CreateClient();
            // Arrange
            var request1 = new
            {
                Url = "/api/DepartmentManagers/authenticate",
                Body = new
                {
                    Email = "string",
                    Password = "string"
                }
            };

            // Act
            var response1 = await client.PostAsync(request1.Url, ContentHelper.GetStringContent(request1.Body));
            var value = await response1.Content.ReadAsStringAsync();
            JObject json = (JObject)JsonConvert.DeserializeObject(value);
            var token = "";
            if (json != null)
            {
                if (json["token"] != null)
                {
                    token = json["token"].ToString();
                }
            }
            var request2 = new
            {
                Url = url,
                Body = new
                {
                    Token = token
                }
            };
            // Assert
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var response2 = await client.GetAsync(request2.Url); //insert identification in body

            // Assert
            response2.EnsureSuccessStatusCode(); // Status Code 200

        }
    }
}
