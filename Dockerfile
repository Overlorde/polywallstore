FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env
WORKDIR /source
COPY ./PolyWallStore/PolyWallStoreApi.csproj ./PolyWallStore/PolyWallStoreApi.csproj
COPY ./PolyWallStore.Tests/PolyWallStoreApi.Tests.csproj ./PolyWallStoreApi.Tests/PolyWallStoreApi.Tests.csproj
COPY ./PolyWallStore.Integrations.Tests/PolyWallStoreApi.IntegrationTests.csproj ./PolyWallStoreApi.Integrations.Tests/PolyWallStoreApi.IntegrationTests.csproj
# Restore
RUN dotnet restore ./PolyWallStore/PolyWallStoreApi.csproj \
& dotnet restore ./PolyWallStoreApi.Tests/PolyWallStoreApi.Tests.csproj \
& dotnet restore ./PolyWallStoreApi.Integrations.Tests/PolyWallStoreApi.IntegrationTests.csproj

# Copy all source code
COPY . .

WORKDIR /source
RUN dotnet test --filter FullyQualifiedName!~IntegrationTests -c Release
RUN dotnet publish -c Release -o /publish
FROM mcr.microsoft.com/dotnet/core/runtime:3.1
WORKDIR /publish
COPY --from=build-env /publish .
ENTRYPOINT ["dotnet", "PolyWallStoreApi.dll"]