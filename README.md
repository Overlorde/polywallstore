# PolyWallStore

A Vue.js application made to manage stocks and employees.

## Prerequisites

- Visual Studio 2019.
- Windows 10.
- SQL Server.
- SQL Server Management Studio 18.

# Getting Started

Once the project is opened in Visual Studio right click on "References" in the Solution Explorer and click on "Manage NuGet Packages".
If Visual Studio informs you that some packages are missing then click on the "Restore" on the right.

Launch the solution PolyWallStore.sln inside PolyWallStore repository.

## Server

The server uses Windows SQL Server to manage the database records and both scripts must be integrated before launch.

1. Launch SQL Server Server Management Studio 
2. Connect with (localdb)\MSSQLLocalDB on username and Windows as identification
3. Create an empty database PolyWallStore
4. Create a new SQL request in SQL Server Management Studio and use database.sql in order to fill the database
5. Repeat the same operation with IntegrationDatabase
6. In order to use the services, use the 'data.txt' and insert the records inside the table DepartmentManager
7. You are now good to go !

# Initialization

An Owner (super-user) has already been created in database as the API is protected by JWT. 
All the rights are granted with that user role and can be modified once identified in the system.

In order to use the swagger, you must use the route /authenticate and write credentials to receive the token that you will have to insert in Authorize.  
__Don't forget to write the authorization that way : "Bearer TOKEN"__  
Once identified, the services will be accessible depending on your role and the associated rights.
Identification :
```
Email : owner@email.com
Password : string
```

## Front-End

To install the front-end, go inside the front-end folder /polywallstore_front-end and write :
```bash
npm install
npm run serve
```
In order to access the front-end use this url :
- http://localhost:8080/

# The services

![Alt text](swagger.png?raw=true "The existing services")